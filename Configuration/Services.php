<?php

declare(strict_types=1);
namespace Getdesigned\Restful;

use Getdesigned\Restful\Mvc\Controller\ActionController;
use Getdesigned\Restful\Mvc\Controller\ControllerInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;

return static function(ContainerConfigurator $containerConfigurator, ContainerBuilder $container) {
    $container->registerForAutoconfiguration(ControllerInterface::class)->addTag('restful.controller');
    $container->registerForAutoconfiguration(ActionController::class)->addTag('restful.action_controller');
    $container->addCompilerPass(new class() implements CompilerPassInterface {
        public function process(ContainerBuilder $container): void
        {
            foreach ($container->findTaggedServiceIds('restful.controller') as $id => $tags) {
                $container->findDefinition($id)->setPublic(true);
            }
            foreach ($container->findTaggedServiceIds('restful.action_controller') as $id => $tags) {
                $container->findDefinition($id)->setShared(false);
            }
        }
    });
};