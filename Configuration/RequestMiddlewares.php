<?php

return [
    'frontend' => [
        'getdesigned/restful/handler' => [
            'target' => \Getdesigned\Restful\Middleware\RequestHandler::class,
            'before' => [
                'typo3/cms-frontend/preview-simulator'
            ],
            'after' => [
                'typo3/cms-frontend/page-resolver'
            ]
        ]
    ]
];