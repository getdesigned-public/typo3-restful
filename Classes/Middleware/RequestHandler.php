<?php

declare(strict_types=1);
namespace Getdesigned\Restful\Middleware;

/*
 * Copyright notice
 *
 * (c) 2023 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

use Getdesigned\Restful\Mvc\Dispatcher;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use TYPO3\CMS\Core\Package\Exception\MissingPackageManifestException;
use TYPO3\CMS\Core\Package\Exception\UnknownPackageException;
use TYPO3\CMS\Extbase\Mvc\Exception\InvalidControllerException;

/**
 * Middleware capable of handling RESTful API endpoints.
 *
 * @author Daniel Haring <dh@getdesigned.at>
 */
class RequestHandler implements MiddlewareInterface
{
    /**
     * The dispatcher to use.
     *
     * @var Dispatcher
     */
    protected Dispatcher $dispatcher;

    /**
     * Dependency Injection.
     *
     * @param Dispatcher $dispatcher The dispatcher to use
     */
    public function __construct(Dispatcher $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    /**
     * @inheritdoc
     * @throws InvalidControllerException
     * @throws MissingPackageManifestException
     * @throws UnknownPackageException
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $routeArguments = $request->getAttribute('routing')->getRouteArguments();
        if (true !== ($routeArguments['restful'] ?? false)) {
            return $handler->handle($request);
        }
        return $this->dispatcher->dispatch($request);
    }
}