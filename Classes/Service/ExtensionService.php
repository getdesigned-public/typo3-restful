<?php

declare(strict_types=1);
namespace Getdesigned\Restful\Service;

/*
 * Copyright notice
 *
 * (c) 2023 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

use stdClass;
use Symfony\Component\Yaml\Yaml;
use TYPO3\CMS\Core\Package\Exception\MissingPackageManifestException;
use TYPO3\CMS\Core\Package\Exception\UnknownPackageException;
use TYPO3\CMS\Core\Package\PackageManager;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Service for handling extensions.
 *
 * @author Daniel Haring <dh@getdesigned.at>
 */
class ExtensionService implements SingletonInterface
{
    /**
     * TYPO3 CMS Package Manager.
     *
     * @var PackageManager
     */
    protected PackageManager $packageManager;

    /**
     * Buffer for class prefixes already resolved.
     *
     * @var string[]|null[]
     */
    private array $classPrefixes = [];

    /**
     * Dependency Injection.
     *
     * @param PackageManager $packageManager TYPO3 CMS Package Manager
     */
    public function __construct(PackageManager $packageManager)
    {
        $this->packageManager = $packageManager;
    }

    /**
     * Resolves the class prefix for the given extension name.
     *
     * @param string $extensionName The name of the extension to resolve the class prefix for
     * @return string The class prefix resolved
     * @throws MissingPackageManifestException
     * @throws UnknownPackageException
     */
    public function getClassPrefix(string $extensionName): string
    {
        if (array_key_exists($extensionName, $this->classPrefixes)) {
            if (null === $this->classPrefixes[$extensionName]) {
                throw new MissingPackageManifestException('Unable to resolve class prefix for extension "' . $extensionName . '". Please specify PSR-4 namespace inside Composer manifest.', 1689867560);
            }
            return $this->classPrefixes[$extensionName];
        }

        $extKey = GeneralUtility::camelCaseToLowerCaseUnderscored($extensionName);

        if (!$this->packageManager->isPackageActive($extKey)) {
            throw new UnknownPackageException('Extension "' . $extensionName . '" does not exist.', 1689865732);
        }

        $package = $this->packageManager->getPackage($extKey);

            // read from composer.json
        $autoload = $package->getValueFromComposerManifest('autoload');
        if ($autoload instanceof stdClass && ($psr = $autoload->{'psr-4'}) instanceof stdClass) {
            foreach ($psr as $classPrefix => $directoryName) {
                if ('Classes' === $directoryName) {
                    $this->classPrefixes[$extensionName] = $classPrefix;
                    return $classPrefix;
                }
            }
        }

            // read from Configuration/Services.yaml
        if (is_readable($yaml = $package->getPackagePath() . 'Configuration/Services.yaml')) {
            $services = Yaml::parseFile($yaml);
            foreach ($services['services'] ?? [] as $classPrefix => $config) {
                if (isset($config['resource']) && '../Classes/*' === $config['resource']) {
                    $this->classPrefixes[$extensionName] = $classPrefix;
                    return $classPrefix;
                }
            }
        }

        throw new MissingPackageManifestException('Unable to resolve class prefix for extension "' . $extensionName . '". Please specify PSR-4 namespace inside Composer manifest.', 1689867560);
    }
}