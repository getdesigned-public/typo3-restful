<?php

declare(strict_types=1);
namespace Getdesigned\Restful\Mvc;

/*
 * Copyright notice
 *
 * (c) 2023 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

use Getdesigned\Restful\Mvc\Controller\ControllerInterface;
use Getdesigned\Restful\Service\ExtensionService;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use TYPO3\CMS\Core\Package\Exception\MissingPackageManifestException;
use TYPO3\CMS\Core\Package\Exception\UnknownPackageException;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Exception\InvalidControllerException;

/**
 * Dispatches requests to the actual controller.
 *
 * @author Daniel Haring <dh@getdesigned.at>
 */
class Dispatcher implements SingletonInterface
{
    /**
     * Service for handling extensions.
     *
     * @var ExtensionService
     */
    protected ExtensionService $extensionService;

    /**
     * Dependency Injection.
     *
     * @param ExtensionService $extensionService Service for handling extensions
     */
    public function __construct(ExtensionService $extensionService)
    {
        $this->extensionService = $extensionService;
    }

    /**
     * Dispatches the given request and returns the response.
     *
     * @param ServerRequestInterface $request The request to dispatch
     * @return ResponseInterface The response
     * @throws InvalidControllerException
     * @throws MissingPackageManifestException
     * @throws UnknownPackageException
     */
    public function dispatch(ServerRequestInterface $request): ResponseInterface
    {
        return $this->resolveController($request)->processRequest($request);
    }

    /**
     * Finds and instantiates a controller that matches the current request.
     *
     * @param ServerRequestInterface $request The request to match
     * @return ControllerInterface The controller resolved
     * @throws InvalidControllerException
     * @throws MissingPackageManifestException
     * @throws UnknownPackageException
     */
    protected function resolveController(ServerRequestInterface $request): ControllerInterface
    {
        $routeArguments = $request->getAttribute('routing')->getRouteArguments();
        $fqcn = sprintf(
            '%1$sRestful\\Controller\\%2$sController',
            $this->extensionService->getClassPrefix((string)($routeArguments['extension'] ?? '')),
            ucfirst((string)($routeArguments['controller'] ?? ''))
        );

        if (!is_a($fqcn, ControllerInterface::class, true)) {
            throw new InvalidControllerException('Invalid controller "' . $fqcn . '". The controller must implement the ' . ControllerInterface::class . '.', 1689868381);
        }

        return GeneralUtility::makeInstance($fqcn);
    }
}