<?php

declare(strict_types=1);
namespace Getdesigned\Restful\Mvc\Controller;

/*
 * Copyright notice
 *
 * (c) 2023 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use RuntimeException;
use Throwable;
use TYPO3\CMS\Core\Error\Http\PageNotFoundException;
use TYPO3\CMS\Core\Error\PageErrorHandler\InvalidPageErrorHandlerException;
use TYPO3\CMS\Core\Error\PageErrorHandler\PageErrorHandlerInterface;
use TYPO3\CMS\Core\Error\PageErrorHandler\PageErrorHandlerNotConfiguredException;
use TYPO3\CMS\Core\Http\JsonResponse;
use TYPO3\CMS\Core\Site\Entity\Site;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\Argument;
use TYPO3\CMS\Extbase\Mvc\Controller\Arguments;
use TYPO3\CMS\Extbase\Mvc\Controller\Exception\RequiredArgumentMissingException;
use TYPO3\CMS\Extbase\Mvc\Exception\InvalidArgumentTypeException;
use TYPO3\CMS\Extbase\Mvc\Exception\NoSuchActionException;
use TYPO3\CMS\Extbase\Mvc\Exception\NoSuchArgumentException;
use TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface;
use TYPO3\CMS\Extbase\Property\Exception\TargetNotFoundException;
use TYPO3\CMS\Extbase\Property\PropertyMapper;
use TYPO3\CMS\Extbase\Reflection\ClassSchema\Exception\NoSuchMethodException;
use TYPO3\CMS\Extbase\Reflection\Exception\UnknownClassException;
use TYPO3\CMS\Extbase\Reflection\ReflectionService;
use TYPO3\CMS\Extbase\Validation\Validator\ConjunctionValidator;
use TYPO3\CMS\Extbase\Validation\Validator\ValidatorInterface;
use TYPO3\CMS\Extbase\Validation\ValidatorResolver;

/**
 * Generic controller for handling RESTful API endpoints.
 *
 * @author Daniel Haring <dh@getdesigned.at>
 */
class ActionController implements ControllerInterface
{
    /**
     * The action method to call.
     *
     * @var string|null
     */
    protected ?string $actionMethodName = null;

    /**
     * The action arguments.
     *
     * @var Arguments
     */
    protected Arguments $arguments;

    /**
     * Name of the special error action method which is called in case of errors.
     *
     * @var string
     */
    protected string $errorMethodName = 'errorAction';

    /**
     * Extbase Persistence Manager.
     *
     * @var PersistenceManagerInterface
     */
    protected PersistenceManagerInterface $persistenceManager;

    /**
     * Service for mapping properties.
     *
     * @var PropertyMapper
     */
    protected PropertyMapper $propertyMapper;

    /**
     * The Extbase reflection service.
     *
     * @var ReflectionService
     */
    protected ReflectionService $reflectionService;

    /**
     * The request being processed.
     *
     * @var ServerRequestInterface|null
     */
    protected ?ServerRequestInterface $request = null;

    /**
     * The response that will be sent.
     *
     * @var ResponseInterface|null
     */
    protected ?ResponseInterface $response = null;

    /**
     * Validator resolver.
     *
     * @var ValidatorResolver
     */
    protected ValidatorResolver $validatorResolver;

    /**
     * Dependency Injection.
     *
     * @param PersistenceManagerInterface $persistenceManager Extbase Persistence Manager
     * @param PropertyMapper $propertyMapper Service for mapping properties
     * @param ReflectionService $reflectionService The Extbase reflection service
     * @param ValidatorResolver $validatorResolver Validator Resolver
     */
    public function __construct(
        PersistenceManagerInterface $persistenceManager,
        PropertyMapper $propertyMapper,
        ReflectionService $reflectionService,
        ValidatorResolver $validatorResolver
    ) {
        $this->persistenceManager = $persistenceManager;
        $this->propertyMapper = $propertyMapper;
        $this->reflectionService = $reflectionService;
        $this->validatorResolver = $validatorResolver;
        $this->arguments = new Arguments();
    }

    /**
     * Processes the request.
     *
     * @param ServerRequestInterface $request The request to process
     * @return ResponseInterface The response of the processed request
     * @throws InvalidArgumentTypeException
     * @throws InvalidPageErrorHandlerException
     * @throws NoSuchActionException
     * @throws NoSuchMethodException
     * @throws PageNotFoundException
     * @throws RequiredArgumentMissingException
     * @throws UnknownClassException
     */
    public function processRequest(ServerRequestInterface $request): ResponseInterface
    {
        $this->request = $request;
        $GLOBALS['TYPO3_REQUEST'] = $GLOBALS['TYPO3_REQUEST'] ?? $request;

        $this->actionMethodName = $this->resolveActionMethodName();
        if (!$this->isMethodAllowed()) {
            return $this->handleError(
                405,
                'Method "' . $request->getMethod() . '" is not allowed for action "'
                    . static::class . '::' . $this->actionMethodName . '".'
            );
        }

        $this->initializeActionMethodArguments();
        $this->initializeActionMethodValidators();

        if (method_exists($this, 'initializeAction')) {
            $this->initializeAction();
        }
        $actionInitializationMethodName = 'initialize' . ucfirst($this->actionMethodName);
        if (method_exists($this, $actionInitializationMethodName)) {
            $this->{$actionInitializationMethodName}();
        }

        $this->response = $this->initializeResponse();

        try {
            $this->mapRequestArgumentsToControllerArguments();
        } catch (Throwable $exception) {
            return $this->error($exception->getCode(), $exception->getMessage());
        }

        return $this->callActionMethod();
    }

    /**
     * A special action which is called if the originally intended action could not be called,
     * for example if the arguments were not valid.
     *
     * @return ResponseInterface The error response
     */
    protected function errorAction(): ResponseInterface
    {
        $errorCode = 0;
        $message = '';

        $validationResults = $this->arguments->validate();
        foreach ($validationResults->getFlattenedErrors() as $propertyPath => $errors) {
            foreach ($errors as $error) {
                /* @var \TYPO3\CMS\Extbase\Validation\Error $error */
                if (empty($errorCode)) {
                    $errorCode = $error->getCode();
                }
                if (!empty($propertyPath)) {
                    $message = $propertyPath . ': ' . $error->getMessage();
                    $errorCode = $error->getCode();
                    break 2;
                }
            }
        }

        return $this->error($errorCode, $message);
    }

    /**
     * Calls the specified action method.
     *
     * @return ResponseInterface The response of the action method called
     */
    protected function callActionMethod(): ResponseInterface
    {
        $preparedArguments = [];

        /* @var Argument $argument */
        foreach ($this->arguments as $argument) {
            $preparedArguments[] = $argument->getValue();
        }

        $validationResult = $this->arguments->validate();
        if (!$validationResult->hasErrors()) {
            $actionResult = $this->{$this->actionMethodName}(...$preparedArguments);
        } else {
            $actionResult = $this->{$this->errorMethodName}();
        }

        $this->persist();

        if ($actionResult instanceof ResponseInterface) {
            return $actionResult;
        }

        $this->applyActionResult($actionResult);
        return $this->response;
    }

    /**
     * Maps arguments delivered by the request object to the local controller arguments.
     *
     * @return void
     * @throws RequiredArgumentMissingException
     * @throws TargetNotFoundException
     */
    protected function mapRequestArgumentsToControllerArguments(): void
    {
        $requestArguments = $this->request->getAttribute('routing')->getArguments();
        if (
            'application/json' === $this->request->getHeaderLine('Content-Type')
            && !empty($rawBody = (string)$this->request->getBody())
        ) {
            try {
                if (is_array($parsedBody = json_decode($rawBody, true))) {
                    $requestArguments = array_merge($requestArguments, $parsedBody);
                }
            } catch (Throwable $exception) {}
        } elseif (is_array($parsedBody = $this->request->getParsedBody())) {
            $requestArguments = array_merge($requestArguments, $parsedBody);
        }
        $requestArguments = array_diff_key(
            $requestArguments,
            array_flip(['action', 'controller', 'extension', 'method', 'restful'])
        );

        /* @var Argument $argument */
        foreach ($this->arguments as $argument) {
            $argumentName = $argument->getName();
            if (isset($requestArguments[$argumentName])) {
                $this->setArgumentValue($argument, $requestArguments[$argumentName]);
                unset($requestArguments[$argumentName]);
            } elseif ($argument->isRequired()) {
                throw new RequiredArgumentMissingException('Required argument "' . $argumentName . '" is not set for ' . static::class . '->' . $this->actionMethodName . '.', 1689859471);
            }
        }

        if (!empty($requestArguments)) {
            foreach ($requestArguments as $argumentName => $_) {
                throw new NoSuchArgumentException('Provided argument "' . $argumentName . '" is not supported.', 1692198667);
            }
        }
    }

    /**
     * Initializes the response.
     *
     * @return ResponseInterface The response initialized
     */
    protected function initializeResponse(): ResponseInterface
    {
        return new JsonResponse();
    }

    /**
     * Registers arguments of the current action.
     *
     * @return void
     * @throws InvalidArgumentTypeException
     * @throws NoSuchMethodException
     * @throws UnknownClassException
     */
    protected function initializeActionMethodArguments(): void
    {
        $methodParameters = $this->reflectionService
            ->getClassSchema(static::class)
            ->getMethod($this->actionMethodName)
            ->getParameters()
        ;

        foreach ($methodParameters as $parameterName => $parameter) {
            $dataType = $parameter->getType();
            if (null === $dataType && $parameter->isArray()) {
                $dataType = 'array';
            }
            if (null === $dataType) {
                throw new InvalidArgumentTypeException('The argument type for parameter $' . $parameterName . ' of method ' . static::class . '->' . $this->actionMethodName . '() could not be detected.', 1689858914);
            }
            $defaultValue = $parameter->hasDefaultValue() ? $parameter->getDefaultValue() : null;
            $this->arguments->addNewArgument($parameterName, $dataType, !$parameter->isOptional(), $defaultValue);
        }
    }

    /**
     * Adds the needed validators to the Arguments.
     *
     * @return void
     */
    protected function initializeActionMethodValidators(): void
    {
        if (0 === $this->arguments->count()) {
            return;
        }

        $classSchemaMethod = $this->reflectionService
            ->getClassSchema(static::class)
            ->getMethod($this->actionMethodName)
        ;

        /* @var Argument $argument */
        foreach ($this->arguments as $argument) {
            $classSchemaMethodParameter = $classSchemaMethod->getParameter($argument->getName());
            if ($classSchemaMethodParameter->ignoreValidation()) {
                continue;
            }
            $validator = GeneralUtility::makeInstance(ConjunctionValidator::class);
            foreach ($classSchemaMethodParameter->getValidators() as $validatorDefinition) {
                if (method_exists($validatorDefinition['className'], 'setOptions')) {
                    /* @var ValidatorInterface $validatorInstance */
                    $validatorInstance = GeneralUtility::makeInstance($validatorDefinition['className']);
                    $validatorInstance->setOptions($validatorDefinition['options']);
                } else {
                    /* @var ValidatorInterface $validatorInstance */
                    $validatorInstance = GeneralUtility::makeInstance(
                        $validatorDefinition['className'],
                        $validatorDefinition['options']
                    );
                }
                $validator->addValidator($validatorInstance);
            }
            $baseValidatorConjunction = $this->validatorResolver->getBaseValidatorConjunction($argument->getDataType());
            if ($baseValidatorConjunction->count() > 0) {
                $validator->addValidator($baseValidatorConjunction);
            }
            $argument->setValidator($validator);
        }
    }

    /**
     * Resolves and checks the current action method name.
     *
     * @return string The action method name resolved
     * @throws NoSuchActionException
     */
    protected function resolveActionMethodName(): string
    {
        $routeArguments = $this->request->getAttribute('routing')->getRouteArguments();
        $actionMethodName = ($routeArguments['action'] ?? '') . 'Action';

        if (!method_exists($this, $actionMethodName)) {
            throw new NoSuchActionException('An action "' . $actionMethodName . '" does not exist in controller "' . static::class . '".', 1689857607);
        }

        return $actionMethodName;
    }

    /**
     * Checks whether the request method is allowed for the current action.
     *
     * @return bool TRUE if the method is allowed, otherwise FALSE
     */
    protected function isMethodAllowed(): bool
    {
        $routeArguments = $this->request->getAttribute('routing')->getRouteArguments();
        return 0 === strcasecmp((string)($routeArguments['method'] ?? 'GET'), $this->request->getMethod());
    }

    /**
     * Applies the action result to the response.
     *
     * @param mixed $actionResult The action result to apply
     * @param int $code The response code to set
     * @param string $message The response message to set, if any
     * @return void
     */
    protected function applyActionResult($actionResult, int $code = 0, string $message = ''): void
    {
        if (!method_exists($this->response, 'setPayload')) {
            return;
        }

        $payload = [
            'code' => $code,
            'message' => $message,
            'type' => $this->response->getStatusCode() >= 400 ? 'error' : 'success'
        ];

        if (null !== $actionResult) {
            $payload['data'] = $actionResult;
        }

        $this->response->setPayload($payload);
    }

    /**
     * Returns an error response.
     *
     * @param int $errorCode The error code to set
     * @param string $message The error message to set
     * @param int $statusCode The HTTP status code to use
     * @return ResponseInterface The error response
     */
    final protected function error(int $errorCode = 0, string $message = '', int $statusCode = 400): ResponseInterface
    {
        $this->setStatusCode($statusCode);
        $this->applyActionResult(null, $errorCode, $message);
        return $this->response;
    }

    /**
     * Sets the HTTP status code of the response.
     *
     * @param int $statusCode The status code to set
     * @return void
     */
    final protected function setStatusCode(int $statusCode): void
    {
        $this->response = $this->response->withStatus($statusCode);
    }

    /**
     * Sets a header of the response.
     *
     * @param string $name The name of the header to set
     * @param string $value The value of the header to set
     * @return void
     */
    final protected function setHeader(string $name, string $value): void
    {
        $this->response = $this->response->withHeader($name, $value);
    }

    /**
     * Sets the value for an argument.
     *
     * @param Argument $argument The argument to set the value for
     * @param mixed $rawValue The value to set
     * @return void
     * @throws TargetNotFoundException
     */
    private function setArgumentValue(Argument $argument, $rawValue): void
    {
        if (null === $rawValue) {
            $argument->setValue(null);
            return;
        }

        $dataType = $argument->getDataType();

        if (is_object($rawValue) && $rawValue instanceof $dataType) {
            $argument->setValue($rawValue);
            return;
        }

        $this->propertyMapper->resetMessages();

        try {
            $argument->setValue($this->propertyMapper->convert(
                $rawValue,
                $dataType,
                $argument->getPropertyMappingConfiguration()
            ));
        } catch (TargetNotFoundException $exception) {
            if ($argument->isRequired()) {
                throw $exception;
            }
        }

        $argument->getValidationResults()->merge($this->propertyMapper->getMessages());
    }

    /**
     * Handles an error.
     *
     * @param int $statusCode The code of the error to handle
     * @param string $message The message to apply
     * @param array $reasons Additional reasons to pass
     * @return ResponseInterface The error response
     * @throws InvalidPageErrorHandlerException
     * @throws PageNotFoundException
     */
    private function handleError(int $statusCode, string $message, array $reasons = []): ResponseInterface
    {
        $errorHandler = $this->getErrorHandlerFromSite($statusCode);
        if ($errorHandler instanceof PageErrorHandlerInterface) {
            return $errorHandler->handlePageError($this->request, $message, $reasons);
        }

        try {
            return $this->handleDefaultError($statusCode, $message);
        } catch (RuntimeException $exception) {
            throw new PageNotFoundException($message, 1689858500);
        }
    }

    /**
     * Ensures that a response object is created as a "fallback" when no error handler is configured.
     *
     * @param int $statusCode The error code to handle
     * @param string $reason The message to apply, if any
     * @return ResponseInterface The error response
     */
    private function handleDefaultError(int $statusCode, string $reason = ''): ResponseInterface
    {
        return new JsonResponse(['error' => $reason], $statusCode);
    }

    /**
     * Persists all prepared data.
     *
     * @return void
     */
    private function persist(): void
    {
        // @todo: store modified session data as well?
        $this->persistenceManager->persistAll();
    }

    /**
     * Checks if a site is configured, and an error handler is configured for this specific status code.
     *
     * @param int $statusCode The error code to return the handler for
     * @return PageErrorHandlerInterface|null The error handler found, if any
     * @throws InvalidPageErrorHandlerException
     */
    private function getErrorHandlerFromSite(int $statusCode): ?PageErrorHandlerInterface
    {
        $site = $this->request->getAttribute('site');
        if ($site instanceof Site) {
            try {
                return $site->getErrorHandler($statusCode);
            } catch (PageErrorHandlerNotConfiguredException $exception) {}
        }
        return null;
    }
}