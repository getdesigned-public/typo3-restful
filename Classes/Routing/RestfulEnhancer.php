<?php

declare(strict_types=1);
namespace Getdesigned\Restful\Routing;

/*
 * Copyright notice
 *
 * (c) 2023 Getdesigned GmbH <office@getdesigned.at>
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 */

use TYPO3\CMS\Core\Routing\Enhancer\PluginEnhancer;
use TYPO3\CMS\Core\Routing\Route;
use TYPO3\CMS\Core\Routing\RouteCollection;

/**
 * Route enhancer for RESTful API endpoints.
 *
 * A typical configuration looks like this:
 *
 * routeEnhancers:
 *   Api:
 *     type: Restful
 *     extension: ApiExample
 *     limitToPages:
 *       - 1
 *     routes:
 *       - { routePath: '/api/foo', _controller: 'Api::foo', _method: 'GET', _arguments: { 'foo': 'foo' } }
 *       - { routePath: '/api/bar', _controller: 'Api::bar', _method: 'POST' }
 *     requirements:
 *       foo: '[0-9]+'
 *
 * @author Daniel Haring <dh@getdesigned.at>
 */
class RestfulEnhancer extends PluginEnhancer
{
    /**
     * The routes configured for the plugin handled.
     *
     * @var array[]
     */
    protected array $routesOfPlugin;

    /**
     * Constructs the route enhancer.
     *
     * @param array $configuration The route enhancer configuration given
     */
    public function __construct(array $configuration)
    {
        parent::__construct($configuration);
        $this->namespace = md5(json_encode($configuration));
        $this->routesOfPlugin = $this->configuration['routes'] ?? [];
    }

    /**
     * @inheritdoc
     */
    public function enhanceForMatching(RouteCollection $collection): void
    {
        $i = 0;

        /* @var Route $defaultPageRoute */
        $defaultPageRoute = $collection->get('default');
        foreach ($this->routesOfPlugin as $configuration) {
            $route = $this->getVariant($defaultPageRoute, $configuration);
            if (!$this->verifyMethod($route)) {
                continue;
            }
            $collection->add($this->namespace . '_' . $i++, $route);
        }
    }

    /**
     * @inheritdoc
     */
    public function enhanceForGeneration(RouteCollection $collection, array $originalParameters): void
    {
        $i = 0;

        /* @var Route $defaultPageRoute */
        $defaultPageRoute = $collection->get('default');

        foreach ($this->routesOfPlugin as $configuration) {
            $variant = $this->getVariant($defaultPageRoute, $configuration);
            if (
                    // Only generation of links to GET requests is possible at the moment
                !$this->verifyMethod($variant, 'GET')
                    // The enhancer tells us: This given route does not match the parameters
                || !$this->verifyRequiredParameters($variant, $originalParameters)
            ) {
                continue;
            }
            $parameters = $originalParameters;
            unset($parameters['action'], $parameters['controller'], $parameters['method']);
            $compiledRoute = $variant->compile();
            $deflatedParameters = $this->deflateParameters($variant, $parameters);
            $variables = array_flip($compiledRoute->getPathVariables());
            $mergedParams = array_replace($variant->getDefaults(), $deflatedParameters);
            // all params must be given, otherwise we exclude this variant
            // (it is allowed that $variables is empty - in this case variables are
            // "given" implicitly through controller-action pair in `_controller`)
            if (array_diff_key($variables, $mergedParams)) {
                continue;
            }
            $variant->addOptions(['deflatedParameters' => $deflatedParameters]);
            $collection->add($this->namespace . '_' . $i++, $variant);
        }
    }

    /**
     * @inheritdoc
     */
    public function inflateParameters(array $parameters, array $internals = []): array
    {
        $parameters = $this->getVariableProcessor()->inflateParameters($parameters) ?? [];
        if (!isset($internals['_controller']) || !isset($this->configuration['extension'])) {
            return $parameters;
        }

        $this->applyControllerActionValues(
            $internals['_controller'],
            $internals['_method'] ?? 'GET',
            $this->configuration['extension'],
            $parameters
        );

        return $parameters;
    }

    /**
     * @inheritdoc
     */
    protected function getVariant(Route $defaultPageRoute, array $configuration): Route
    {
        $arguments = $configuration['_arguments'] ?? [];
        unset($configuration['_arguments']);

        $variableProcessor = $this->getVariableProcessor();
        $routePath = $this->modifyRoutePath($configuration['routePath']);
        $routePath = $variableProcessor->deflateRoutePath($routePath, '', $arguments);
        unset($configuration['routePath']);
        $defaults = array_merge_recursive($defaultPageRoute->getDefaults(), $configuration);
        $options = array_merge(
            $defaultPageRoute->getOptions(),
            ['_enhancer' => $this, 'utf8' => true, '_arguments' => $arguments]
        );
        $route = new Route(
            rtrim($defaultPageRoute->getPath(), '/') . '/' . ltrim($routePath, '/'),
            $defaults,
            [],
            $options
        );

        $this->applyRouteAspects($route, $this->aspects ?? [], '');
        $this->applyRequirements($route, $this->configuration['requirements'] ?? [], '');

        return $route;
    }

    /**
     * Checks whether the given route is applicable for a specific HTTP method.
     *
     * @param Route $route The route to check
     * @param string|null $method The HTTP method the route has to be applicable for.
     *                            If omitted, the request method will be taken into account.
     * @return bool TRUE if the route is applicable for the HTTP method specified, otherwise FALSE
     */
    protected function verifyMethod(Route $route, string $method = null): bool
    {
        if (!$route->hasDefault('_method')) {
            return false;
        }

        $targetMethod = strtoupper($method ?? $_SERVER['REQUEST_METHOD'] ?? 'GET');

        return strtoupper($route->getDefault('_method')) === $targetMethod;
    }

    /**
     * Checks whether all required parameters are set and valid.
     *
     * @param Route $route The route to check the parameters for
     * @param array $parameters The parameters to check
     * @return bool TRUE if all required parameters are set and valid, otherwise FALSE
     */
    protected function verifyRequiredParameters(Route $route, array $parameters): bool
    {
        if (!$route->hasDefault('_controller')) {
            return false;
        }
        if (!$route->hasDefault('_method')) {
            return false;
        }
        $controller = $route->getDefault('_controller');
        [$controllerName, $actionName] = explode('::', $controller);
        if ($controllerName !== $parameters['controller']) {
            return false;
        }
        if ($actionName !== $parameters['action']) {
            return false;
        }
        return true;
    }

    /**
     * Add controller and action parameters, so they can be used later-on.
     *
     * @param string $controllerActionValue The Controller::action combination given
     * @param string $methodName The method given
     * @param string $extensionName The extension name given
     * @param array $target Reference to target array to be modified
     * @return void
     */
    protected function applyControllerActionValues(
        string $controllerActionValue,
        string $methodName,
        string $extensionName,
        array &$target
    ): void {
        if (!str_contains($controllerActionValue, '::')) {
            return;
        }
        [$controllerName, $actionName] = explode('::', $controllerActionValue, 2);
        $target['controller'] = $controllerName;
        $target['action'] = $actionName;
        $target['method'] = $methodName;
        $target['extension'] = $extensionName;
        $target['restful'] = true;
    }
}