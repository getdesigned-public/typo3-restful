<?php
$EM_CONF[$_EXTKEY] = [
    'title' => 'RESTful API',
    'description' => 'Framework for building your own TYPO3 CMS RESTful API',
    'category' => 'fe',
    'version' => '11.0.5-dev',
    'state' => 'stable',
    'author' => 'Daniel Haring',
    'author_company' => 'Getdesigned GmbH',
    'author_email' => 'dh@getdesigned.at',
    'constraints' => [
        'depends' => [
            'core' => '11.5.0-11.99.99',
            'extbase' => '11.5.0-11.99.99'
        ]
    ]
];