# RESTful API Framework for TYPO3 CMS

This extension facilitates easy mapping of HTTP methods to controller actions.

## Creating a controller

A RESTful controller is created in `~/Classes/Restful/Controller/` directory of an extension and must implement 
`\Getdesigned\Restful\Mvc\Controller\ControllerInterface` – but typically derives directly from 
`\Getdesigned\Restful\Mvc\Controller\ActionController`, which implements the interface and takes care of calling the 
correct action.

For example, a simplified controller for standard CRUD operations for books might look like this:

```php
<?php

declare(strict_types=1);
namespace VendorName\MyExt\Restful\Controller;

use Getdesigned\Restful\Mvc\Controller\ActionController;
use VendorName\MyExt\Domain\Model\Book;

class BookController extends ActionController
{
    /**
     * Lists all available books. 
     * 
     * @return array All available books
     */
    public function indexAction(): array
    {    
        // …
    }

    /** 
     * Shows details of a single book.
     * 
     * @param int $book The identifier of the book to show details of
     * @return Book Details of the book found
     */
    public function viewAction(int $book): Book
    {
        // …
    }

    /**
     * Creates a new book. 
     * 
     * @param array $book The data of the book to create
     * @return Book The created book
     */
    public function createAction(array $book): Book
    {
        // …
    }

    /**
     * Updates an existing book.
     * 
     * @param array $book The data of the book to update
     * @return Book The updated book
     */
    public function updateAction(array $book): Book
    {
        // …
    }

    /**
     * Deletes an existing book.
     * 
     * @param int $book The identifier of the book to delete
     * @return bool Success
     */
    public function deleteAction(int $book): bool
    {
       // …
    }
}
```

## Mapping HTTP methods to actions

HTTP methods can be mapped to controller actions via the *Restful Route Enhancer* like so:

```yaml
routeEnhancers:
  Api:
    type: Restful
    extension: MyExt
    limitToPages:
      - 1
    routes:
      -
        routePath: /api/books
        _controller: 'Book::index'
        _method: GET
      -
        routePath: /api/books/{book_id}
        _controller: 'Book::view'
        _method: GET
        _arguments:
          book_id: book
      -
        routePath: /api/books
        _controller: 'Book::create'
        _method: POST
      -
        routePath: /api/books
        _controller: 'Book::update'
        _method: PATCH
      -
        routePath: /api/books
        _controller: 'Book::delete'
        _method: DELETE
```